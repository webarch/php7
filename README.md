# Debian Stretch PHP 

This repo contains GitLab CI instructions to install Ansible and GPG and SSH secret keys to a Docker container and then run an Ansible playbook, `run.yml`, which runs three roles:

1.  The build role download source debs for Apache2, OpenSSL and  PHP7.0, set the file descriptors limit to a higher value and then rebuilds and signs them and creates an apt repo layout.
2.  The test role then installs the debs that have been built to test that they can be installed without errors and also checks the complied value of `enable-fd-setsize`.
3.  The deploy role `rsync`'s the files to the apt repo.

The apt repo that this code generates is available at [deb.webarch.net](https://deb.webarch.net/), where you can also find instructions for using it.

The variables in `run.yml` would need changing if these playbooks are used elsewhere or used for building other packages.

**Note that before compiling a new version the number of Debian PHP7.0 patches
needs to be checked** &mdash; the `php_patch_number` in `run.yml` might need
incrementing if there are more than *49*, you can check the patches by going
to the [Debian PHP7.0 package page](https://packages.debian.org/stretch/php7.0)
and then following the link in the right hand column to the 
*Debian Patch Tracker*, version `7.0.33-0+deb9u1` has 
[49 patches](https://sources.debian.org/patches/php7.0/7.0.33-0+deb9u1/), so for
this version the `php_patch_number` in `run.yml` is set to `50`, the patch that
this code applies, using `quilt`, is generated from 
[this template](https://git.coop/webarch/php/blob/master/roles/build/templates/fd-setsize.patch.j2).

A [blog post](https://www.blog.webarchitects.coop/2019/02/building-debian-php-packages-with-gitlab-ci-and-ansible) has been written about this.

# Environment variables

The following environmental variables are set in GitLab CI / CD Settings for this project:

* GPG_PRIVATE_KEY 
* SSH_PRIVATE_KEY
* SSH_KNOWN_HOSTS

It would be more secure to use Ansible Vault or something&hellip;

The corresponding GPG public key is in `roles/build/files/pub.gpg` and the GPG key id is a variable in `run.yml`.

The corresponding SSH public key is installed on the host that the repo is rsync'ed to.

# PHP File Descriptors Limit

This is the issue we hit on shared web servers with 50+ clients when trying to send email with InvoicePlane:

```
A PHP Error was encountered                                                                                                                          
                                                                                                                                                     
Severity: Warning                                                                                                                                    
                                                                                                                                                     
Message: stream_select(): You MUST recompile PHP with a larger value of
FD_SETSIZE. It is set to 1024, but you have descriptors numbered at least as
high as 1056. --enable-fd-setsize=2048 is recommended, but you may want to set
it to equal the maximum number of open files supported by your system, in order
to avoid seeing this error again at a later date.                                                                                            
                                                                                                                                                     
Filename: src/SMTP.php                                                                                                                               
                                                                                                                                                     
Line Number: 1124                                                                                                                                    
```

However it is not just a matter of recompiling with `--enable-fd-setsize=2048`, see [this bug thread](https://bugs.php.net/bug.php?id=69637), so we also add the following to `main/php.h`:

```
#undef __FD_SETSIZE
#define __FD_SETSIZE 2048
#undef FD_SETSIZE
#define FD_SETSIZE 2048
```

And edit this line in `/usr/include/x86_64-linux-gnu/bits/typesizes.h`:

```
#define __FD_SETSIZE            1024
```

And this line in `/usr/include/linux/posix_types.h`:

```
#define __FD_SETSIZE    1024
```

Note that PHP 7.1.0 onwards has a variable, `PHP_FD_SETSIZE`, see [this bug](https://bugs.php.net/bug.php?id=43269). 

It was found that [the Plesk advice](https://docs.plesk.com/en-US/onyx/advanced-administration-guide-linux/enhancing-performance/increasing-the-number-of-domains-that-plesk-can-serve.68766/) to also compile openssl and  apache2 was needed but the libc-client didn't compile without errors so has been omitted.

The [test script](https://git.coop/webarch/php/blob/master/roles/test/files/test.php) from [PHP bug 69637](https://bugs.php.net/bug.php?id=69637) 
always fails when run in Docker via GitLab CI but works when run on regular virtual server, we are not sure of the reason for this, but this is why the test is 
[commented out](https://git.coop/webarch/php/blob/master/roles/test/tasks/main.yml#L28).

Our shared hosting servers use a lot of file descriptors, for example the total number of open file handles of any sort:

```bash
lsof | wc -l
181379
```

The file descriptors in kernel memory:

```bash
sysctl fs.file-nr
fs.file-nr = 6688       0       1633852
```

Where 6688 is the number of allocated file handles, 0 is the number of unused-but-allocated file handles and 1633852 is the system-wide maximum number of file handles.

Display the maximum number of open file descriptors:

```bash
cat /proc/sys/fs/file-max
1633852
```

Set the maximum number of open file descriptors:

```
sysctl -w fs.file-max=1633852
```

Add the following line to `/etc/sysctl.conf` to ensure that this persists across reboots:

```
fs.file-max = 1633852
``` 

# GPG keypair

After multiple attempts to use a key pair with a passphrase a pair without one was generated:

```bash
gpg --full-generate-key
  gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
  This is free software: you are free to change and redistribute it.
  There is NO WARRANTY, to the extent permitted by law.
  
  gpg: directory '/home/foo/.gnupg' created
  gpg: keybox '/home/foo/.gnupg/pubring.kbx' created
  Please select what kind of key you want:
     (1) RSA and RSA (default)
     (2) DSA and Elgamal
     (3) DSA (sign only)
     (4) RSA (sign only)
  Your selection? 1
  RSA keys may be between 1024 and 4096 bits long.
  What keysize do you want? (3072) 4096
  Requested keysize is 4096 bits
  Please specify how long the key should be valid.
           0 = key does not expire
        <n>  = key expires in n days
        <n>w = key expires in n weeks
        <n>m = key expires in n months
        <n>y = key expires in n years
  Key is valid for? (0) 0
  Key does not expire at all
  Is this correct? (y/N) Y
  
  GnuPG needs to construct a user ID to identify your key.
  
  Real name: Webarchitects Co-operative Debian Packages
  Email address: deb@webarch.net
  Comment: 
  You selected this USER-ID:
      "Webarchitects Co-operative Debian Packages <deb@webarch.net>"
  
  Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
  We need to generate a lot of random bytes. It is a good idea to perform
  some other action (type on the keyboard, move the mouse, utilize the
  disks) during the prime generation; this gives the random number
  generator a better chance to gain enough entropy.
  We need to generate a lot of random bytes. It is a good idea to perform
  some other action (type on the keyboard, move the mouse, utilize the
  disks) during the prime generation; this gives the random number
  generator a better chance to gain enough entropy.
  gpg: /home/foo/.gnupg/trustdb.gpg: trustdb created
  gpg: key 495739F6CAA6F12D marked as ultimately trusted
  gpg: directory '/home/foo/.gnupg/openpgp-revocs.d' created
  gpg: revocation certificate stored as '/home/foo/.gnupg/openpgp-revocs.d/EB612F9FE81381F8F3E58874495739F6CAA6F12D.rev'
  public and secret key created and signed.
  
  pub   rsa4096 2019-02-02 [SC]
        EB612F9FE81381F8F3E58874495739F6CAA6F12D
        EB612F9FE81381F8F3E58874495739F6CAA6F12D
  uid                      Webarchitects Co-operative Debian Packages <deb@webarch.net>
  sub   rsa4096 2019-02-02 [E]
gpg --export --armor EB612F9FE81381F8F3E58874495739F6CAA6F12D > pub.gpg
gpg --export-secret-keys --armor EB612F9FE81381F8F3E58874495739F6CAA6F12D > sec.gpg
```

## References

Building `.deb` files generates a lot of output, time outs needed to be increased, [see this](https://git.coop/webarch/gitlab/commit/ea261dd42f08312e402f24441c134a8b9c08b232).

* [Building debian packages with debuild](https://blog.packagecloud.io/debian/debuild/packaging/2015/06/08/buildling-deb-packages-with-debuild/)
* [Dch non-interactive mode](https://askubuntu.com/questions/579323/dch-non-interactive-mode)
* [HOWTO: GPG sign and verify deb packages and APT repositories](https://blog.packagecloud.io/eng/2014/10/28/howto-gpg-sign-verify-deb-packages-apt-repositories/)
* [DebianRepositorySetupWithReprepro](https://wiki.debian.org/DebianRepository/SetupWithReprepro)
* [Apt Repositories: Goodbye Aptly, Welcome RepRepro](https://www.preining.info/blog/2018/10/aptly-reprepro/)
* [Creating your own Signed APT Repository and Debian Packages](http://blog.jonliv.es/blog/2011/04/26/creating-your-own-signed-apt-repository-and-debian-packages/)
* [reprepro - Is there any chance to enter the passphrase via bash-script?](https://askubuntu.com/a/751916)
* [Using Quilt](https://wiki.debian.org/UsingQuilt)
* [How to patch and build a debian package](https://cs-people.bu.edu/doucette/xia/guides/debian-patch.txt)

GPG / Ansible / GitLab CI:

* [Secrets with Ansible: Ansible Vault and GPG](https://benincosa.com/?p=3235)
* [ansible role to generate gpg keys](https://github.com/juju4/ansible-gpgkey_generate)
* [Ansible GnuPG Integration (for SSH keys, vault passphrases and others)](https://github.com/yaegashi/ansible-snippets/tree/master/gnupg)
* [GitLab CI, unable to load GPG private key](https://stackoverflow.com/questions/43719905/blackbox-on-gitlab-ci-unable-to-load-gpg-private-key)
