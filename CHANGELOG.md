*   2020-07-05  Update for [Debian Security Advisory DSA-4717-1](https://lists.debian.org/debian-security-announce/2020/msg00122.html)
*   2020-02-18  Update for [Debian Security Advisory DSA-4628-1](https://lists.debian.org/debian-security-announce/2020/msg00031.html)
*   2019-10-28  Update for [Debian Security Advisory DSA 4552-1](https://lists.debian.org/debian-security-announce/2019/msg00204.html)
*   2019-10-15  Update for [Debian Security Advisory DSA 4509-3](https://lists.debian.org/debian-security-announce/2019/msg00195.html)
*   2019-09-20  Update for [Debian Security Advisory DSA 4529-1](https://lists.debian.org/debian-security-announce/2019/msg00177.html)
*   2019-04-03  Update for https://httpd.apache.org/security/vulnerabilities_24.html#CVE-2019-0211
*   2019-03-08  Update for Debian Security Advisory DSA-4403-1
*   2019-03-01  Update for CVE-2019-9020 CVE-2019-9021 CVE-2019-9022 CVE-2019-9023 CVE-2019-9024
