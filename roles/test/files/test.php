<?php
/*
 / test script from:
 / https://bugs.php.net/bug.php?id=69637
*/
$pid = getmypid();
$sockets = array();
$loopNbr = 1025; // This normally could go up to the compile --enable-fd-setsize value, but the limit will be anyway 1024, like if it has been hardcoded somewhere
$sysLimit = min(posix_getrlimit()['soft openfiles'], posix_getrlimit()['hard openfiles']);
$nbrInitialOpenedFiles = (int)exec("ls /proc/$pid/fd | wc -l");
echo "There is initially $nbrInitialOpenedFiles opened files".PHP_EOL."The sysLimit is $sysLimit".PHP_EOL."In some few seconds, we will try to open $loopNbr sockets".PHP_EOL;
sleep(5);
for ($i = 0; $i < $loopNbr; $i++) {
    echo $i > ($sysLimit - $nbrInitialOpenedFiles) ? 'Normal crash : openfiles system limitation. Edit /etc/security/limits.conf, increasing soft & hard values' . PHP_EOL : null;
    $s = stream_socket_client("127.0.0.1:80", $errno, $errstr, 30, STREAM_CLIENT_ASYNC_CONNECT | STREAM_CLIENT_CONNECT);
    if ($s) {
        $sockets[$i] = $s;
        echo 'Stream n°' . $i . ' opened' . PHP_EOL;
        echo(exec("lsof -i -a -p $pid") . PHP_EOL);
    } else {exit;}
}
echo ($loopNbr + $nbrInitialOpenedFiles) > 1024 ? 'Weird crash : ($loopNbr + $nbrInitialOpenedFiles) is > 1024' . PHP_EOL : 'Nbr of files descriptor <= 1024 <= sysLimit, then all should be ok' . PHP_EOL;
$read = $sockets;
stream_select($read, $w = null, $e = null, null, null);
